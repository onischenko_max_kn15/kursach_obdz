//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KursovoyOtdelKadrov.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class POST
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public POST()
        {
            this.WORKERs = new HashSet<WORKER>();
        }
    
        public string name { get; set; }
        public string responsobility { get; set; }
        public int workHours { get; set; }
        public decimal wage { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WORKER> WORKERs { get; set; }

        public override string ToString()
        {
            return name;
        }
    }
}
