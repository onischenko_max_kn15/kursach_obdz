﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using KursovoyOtdelKadrov.Models;

namespace KursovoyOtdelKadrov.ViewModels.Workers
{
    public class AddWorkerViewModel : Screen, IDataErrorInfo
    {
        private readonly DepartamentEntities context = new DepartamentEntities();

        private WORKER worker;

        public WORKER Worker
        {
            get { return worker; }
            set
            {
                worker = value;
            }
        }

        public void AddWorker()
        {

            context.WORKERs.Add(worker);
            context.SaveChanges();
        }

        public AddWorkerViewModel()
        {
            posts = new BindableCollection<POST>(context.POSTs.ToList());
            projects = new BindableCollection<PROJECT>(context.PROJECTs.ToList());
            subdivisions = new BindableCollection<SUBDIVISION>(context.SUBDIVISIONs.ToList());
            worker = new WORKER();
        }

        private BindableCollection<POST> posts;

        public BindableCollection<POST> Posts
        {
            get { return posts; }
            set { posts = value; }
        }

        private BindableCollection<PROJECT> projects;

        public BindableCollection<PROJECT> Projects
        {
            get { return projects; }
            set { projects = value; }
        }

        private BindableCollection<SUBDIVISION> subdivisions;

        public BindableCollection<SUBDIVISION> Subdivisions
        {
            get { return subdivisions; }
            set { subdivisions = value; }
        }


        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string result = null;
                if (columnName == "firstName")
                {
                    if (string.IsNullOrEmpty(Worker.firstName))
                        result = "Please enter a first name";
                }
                if (columnName == "secondName")
                {
                    if (string.IsNullOrEmpty(Worker.secondName))
                        result = "Please enter a second name";
                }
                if (columnName == "name")
                {
                    if (string.IsNullOrEmpty(Worker.PROJECT1.name))
                        result = "Please enter a project name";
                }
                return result;
            }
        }
    }
}
