﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using KursovoyOtdelKadrov.Models;

namespace KursovoyOtdelKadrov.ViewModels.Workers
{
    public class UpdateWorkerViewModel : Screen
    {
        private readonly DepartamentEntities context = new DepartamentEntities();

        private WORKER worker;

        public WORKER Worker
        {
            get { return worker; }
            set { worker = value; }
        }

        public void Update()
        {
            context.SaveChanges();
            TryClose(true);
        }

        public UpdateWorkerViewModel(int id)
        {
            worker = context.WORKERs.Find(id);
            posts = new BindableCollection<POST>(context.POSTs.ToList());
            projects = new BindableCollection<PROJECT>(context.PROJECTs.ToList());
            subdivisions = new BindableCollection<SUBDIVISION>(context.SUBDIVISIONs.ToList());
        }

        private BindableCollection<POST> posts;

        public BindableCollection<POST> Posts
        {
            get { return posts; }
            set { posts = value; }
        }

        private BindableCollection<PROJECT> projects;

        public BindableCollection<PROJECT> Projects
        {
            get { return projects; }
            set { projects = value; }
        }

        private BindableCollection<SUBDIVISION> subdivisions;

        public BindableCollection<SUBDIVISION> Subdivisions
        {
            get { return subdivisions; }
            set { subdivisions = value; }
        }

    }
}
