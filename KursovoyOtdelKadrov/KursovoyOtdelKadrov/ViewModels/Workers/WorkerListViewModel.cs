﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using KursovoyOtdelKadrov.Models;
using KursovoyOtdelKadrov.ViewModels.Dialogs;

namespace KursovoyOtdelKadrov.ViewModels.Workers
{
    public class WorkerListViewModel : Screen
    {
        private DepartamentEntities context = new DepartamentEntities();

        private WORKER selectedWorker;

        public WORKER SelectedWorker
        {
            get { return selectedWorker; }
            set
            {
                selectedWorker = value;
                NotifyOfPropertyChange(() => SelectedWorker);
            }
        }


        private BindableCollection<WORKER> workers;

        public BindableCollection<WORKER> Workers
        {
            get { return workers; }
            set
            {
                workers = value;
                NotifyOfPropertyChange(() => Workers);
            }
        }


        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set { searchText = value; }
        }

        public void Search()
        {
            var searchQuery = from searchItem in context.WORKERs
                              where searchItem.secondName.Contains(searchText)
                              select searchItem;
            Workers = new BindableCollection<WORKER>(searchQuery.ToList());
        }




        public void SaveChanges()
        {
            context.SaveChanges();
        }


        public WorkerListViewModel()
        {
            workers = new BindableCollection<WORKER>(context.WORKERs.ToList());
        }

        public void AddWorker()
        {
            if (selectedWorker == null)
                return;
            IWindowManager wm = new WindowManager();
            UpdateWorkerViewModel updateWorkerViewModel = new UpdateWorkerViewModel(selectedWorker.idCard);

            if ((bool)wm.ShowDialog(updateWorkerViewModel, null, null))
            {
                context = new DepartamentEntities();
                Workers = new BindableCollection<WORKER>(context.WORKERs.ToList());
            }
        }

        public void DeleteWorker()
        {
            if (selectedWorker == null)
                return;

            //crete instance of wm and vm
            IWindowManager windowManager = new WindowManager();
            RemoveDialogViewModel removeDialog = new RemoveDialogViewModel();

            
            dynamic settings = new ExpandoObject();
            settings.Title = "Видалити робітника?";

            
            if (windowManager.ShowDialog(removeDialog, null, settings))
            {
                context.WORKERs.Remove(selectedWorker);
                context.SaveChanges();
                Workers.Remove(selectedWorker);
            }

        }

    }
}
