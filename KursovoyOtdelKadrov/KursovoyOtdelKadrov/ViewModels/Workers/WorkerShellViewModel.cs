﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace KursovoyOtdelKadrov.ViewModels.Workers
{
    public class WorkerShellViewModel : Conductor<object>
    {
        public WorkerShellViewModel()
        {
            ActivateItem(new WorkerListViewModel());
        }

        public void GoToAddWorker()
        {
            ActivateItem(new AddWorkerViewModel());
        }
    }
}
