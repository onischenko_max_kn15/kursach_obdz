﻿using Caliburn.Micro;
using System;
using LiveCharts;
using LiveCharts.Wpf;
using KursovoyOtdelKadrov.Models;
using System.Collections.Generic;
using System.Linq;

namespace KursovoyOtdelKadrov.ViewModels.Statistics
{
    class WorkerStatisticViewModel : Screen
    {
        private readonly DepartamentEntities context = new DepartamentEntities();

        public WorkerStatisticViewModel()
        {
            IList<PROJECT> contacts = new List<PROJECT>(context.PROJECTs.ToList());

            SeriesCollection = new SeriesCollection();

            foreach (var contact in contacts)
            {
                ColumnSeries col = new ColumnSeries();
                col.Title = contact.name;
                col.Values = new ChartValues<int> { contact.WORKERs.Count };
                SeriesCollection.Add(col);
            }


            Labels = new string[context.PROJECTs.Count()]; 

            int i = 0;
            foreach (var contact in contacts)
            {
                Labels[i] = contact.name;
            }

            Formatter = value => value.ToString("N");
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }
    }
}
