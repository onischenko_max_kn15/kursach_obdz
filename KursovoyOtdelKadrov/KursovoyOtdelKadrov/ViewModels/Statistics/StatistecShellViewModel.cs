﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KursovoyOtdelKadrov.ViewModels.Statistics
{
    class StatistecShellViewModel : Conductor<object>
    {
        private string selectedPage = null;
        public string SelectedPage
        {
            get { return selectedPage; }
            set
            {
                selectedPage = value;
                Navigate();
            }
        }

        public string[] Pages
        {
            get { return new string[] { "Проекти", "Підрозділи", "Посади" }; }
            set { }
        }

        public void Navigate()
        {
            switch (SelectedPage)
            {
                case "Проекти":
                    ActivateItem(new KursovoyOtdelKadrov.ViewModels.Statistics.WorkerStatisticViewModel());
                    break;
                case "Підрозділи":
                    ActivateItem(new KursovoyOtdelKadrov.ViewModels.Statistics.SubdivisionStatisticViewModel());
                    break;
                case "Посади":
                    ActivateItem(new KursovoyOtdelKadrov.ViewModels.Statistics.PostStatisticViewModel());
                    break;
                default:
                    break;
            }
        }
    }
}