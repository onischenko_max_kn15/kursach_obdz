﻿using Caliburn.Micro;
using System;
using LiveCharts;
using LiveCharts.Wpf;
using KursovoyOtdelKadrov.Models;
using System.Collections.Generic;
using System.Linq;

namespace KursovoyOtdelKadrov.ViewModels.Statistics
{
    class SubdivisionStatisticViewModel : Screen
    {
        private readonly DepartamentEntities context = new DepartamentEntities();

        public SubdivisionStatisticViewModel()
        {
            IList<SUBDIVISION> contacts = new List<SUBDIVISION>(context.SUBDIVISIONs.ToList());

            SeriesCollection = new SeriesCollection();

            foreach (var contact in contacts)
            {
                ColumnSeries col = new ColumnSeries();
                col.Title = contact.name;
                col.Values = new ChartValues<int> { contact.WORKERs1.Count };
                SeriesCollection.Add(col);
            }


            Labels = new string[context.SUBDIVISIONs.Count()];

            int i = 0;
            foreach (var contact in contacts)
            {
                Labels[i] = contact.name;
            }

            Formatter = value => value.ToString("N");
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }
    }
}
