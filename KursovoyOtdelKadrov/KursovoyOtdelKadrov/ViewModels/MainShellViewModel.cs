﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace KursovoyOtdelKadrov.ViewModels
{
    public class MainShellViewModel : Conductor<object>
    {
        public void GoToWorkerShell()
        {
            ActivateItem(new KursovoyOtdelKadrov.ViewModels.Workers.WorkerShellViewModel());
        }

        public void GoToSubdivisionShell()
        {
            ActivateItem(new KursovoyOtdelKadrov.ViewModels.Subdivisions.SubdivisionShellViewModel());
        }

        public void GoToStatisticShell()
        {
            ActivateItem(new KursovoyOtdelKadrov.ViewModels.Statistics.StatistecShellViewModel());
        }
    }
}