﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using KursovoyOtdelKadrov.Models;

namespace KursovoyOtdelKadrov.ViewModels.Subdivisions
{
    public class AddSubdivisionViewModel : Screen
    {
        private readonly DepartamentEntities context = new DepartamentEntities();

        private SUBDIVISION subdivision;

        public SUBDIVISION Subdivision
        {
            get { return subdivision; }
            set
            {
                subdivision = value;
            }
        }

        public void AddSubdivision()
        {

            context.SUBDIVISIONs.Add(subdivision);
            context.SaveChanges();
        }

        public AddSubdivisionViewModel()
        {
            subdivision = new SUBDIVISION();
        }

    }
}
