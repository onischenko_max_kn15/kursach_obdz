﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using KursovoyOtdelKadrov.Models;
using KursovoyOtdelKadrov.ViewModels.Dialogs;

namespace KursovoyOtdelKadrov.ViewModels.Subdivisions
{
    public class SubdivisionListViewModel : Screen
    {
        private DepartamentEntities context = new DepartamentEntities();

        private SUBDIVISION selectedSubdivision;

        public SUBDIVISION SelectedSubdivision
        {
            get { return selectedSubdivision; }
            set
            {
                selectedSubdivision = value;
                NotifyOfPropertyChange(() => SelectedSubdivision);
            }
        }


        private BindableCollection<SUBDIVISION> subdivisions;

        public BindableCollection<SUBDIVISION> Subdivisions
        {
            get { return subdivisions; }
            set
            {
                subdivisions = value;
                NotifyOfPropertyChange(() => Subdivisions);
            }
        }


        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set { searchText = value; }
        }

        public void Search()
        {
            var searchQuery = from searchItem in context.SUBDIVISIONs
                              where searchItem.name.Contains(searchText)
                              select searchItem;
            Subdivisions = new BindableCollection<SUBDIVISION>(searchQuery.ToList());
        }


        public void SaveChanges()
        {
            context.SaveChanges();
        }


        public SubdivisionListViewModel()
        {
            subdivisions = new BindableCollection<SUBDIVISION>(context.SUBDIVISIONs.ToList());
        }

        public void AddSubdivision()
        {
            if (selectedSubdivision == null)
                return;
            IWindowManager wm = new WindowManager();
            UpdateSubdivisionViewModel updateSubdivisionViewModel = new UpdateSubdivisionViewModel(selectedSubdivision.name);

            if ((bool)wm.ShowDialog(updateSubdivisionViewModel, null, null))
            {
                context = new DepartamentEntities();
                Subdivisions = new BindableCollection<SUBDIVISION>(context.SUBDIVISIONs.ToList());
            }
        }

        public void DeleteSubdivision()
        {
            if (selectedSubdivision == null)
                return;

            //crete instance of wm and vm
            IWindowManager windowManager = new WindowManager();
            RemoveDialogViewModel removeDialog = new RemoveDialogViewModel();


            dynamic settings = new ExpandoObject();
            settings.Title = "Видалити робітника?";


            if (windowManager.ShowDialog(removeDialog, null, settings))
            {
                context.SUBDIVISIONs.Remove(selectedSubdivision);
                context.SaveChanges();
                Subdivisions.Remove(selectedSubdivision);
            }

        }
    }
}
