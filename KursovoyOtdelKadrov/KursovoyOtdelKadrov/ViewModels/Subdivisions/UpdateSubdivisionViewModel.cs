﻿using Caliburn.Micro;
using KursovoyOtdelKadrov.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KursovoyOtdelKadrov.ViewModels.Subdivisions
{
    public class UpdateSubdivisionViewModel :Screen
    {
        private readonly DepartamentEntities context = new DepartamentEntities();

        private SUBDIVISION subdivision;

        public SUBDIVISION Subdivision
        {
            get { return subdivision; }
            set { subdivision = value; }
        }

        public void Update()
        {
            context.SaveChanges();
            TryClose(true);
        }

        public UpdateSubdivisionViewModel(string name)
        {
            subdivision = context.SUBDIVISIONs.Find(name);
        }
    }
}
