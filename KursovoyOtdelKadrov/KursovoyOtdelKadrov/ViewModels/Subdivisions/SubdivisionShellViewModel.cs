﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace KursovoyOtdelKadrov.ViewModels.Subdivisions
{
    public class SubdivisionShellViewModel : Conductor<object>
    {
        public SubdivisionShellViewModel()
        {
            ActivateItem(new SubdivisionListViewModel());
        }

        public void GoToAddSubdivision()
        {
            ActivateItem(new AddSubdivisionViewModel());
        }
    }
}
